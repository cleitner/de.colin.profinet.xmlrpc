#!/usr/bin/env python3

import xmlrpc.client, time, json
from xmlrpc.client import Binary as B

class Base64JSONEncoder(json.JSONEncoder):
	def default(self, o):
		if isinstance(o, bytes):
			import binascii
			return binascii.hexlify(o).decode("ASCII")
		elif isinstance(o, B):
			import binascii
			return binascii.hexlify(o.data).decode("ASCII")
		return json.JSONEncoder.default(self, o)

# -- 8< -----------------------------------------------------------------------

client = xmlrpc.client.ServerProxy("http://127.0.0.1:47819/")

if client.profinet.isStopped():
	client.profinet.start("172.21.0.50")

print("Station 'unknown':", client.profinet.findStationByName("unknown"))
print("Station 'mein-fb':", client.profinet.findStationByName("mein-fb"))
print("Cached stations:", client.profinet.getCachedStations())

client.profinet.signalStation("00:0E:F0:05:06:A9")

if not client.profinet.isConnected():
	print("Connecting")

	client.profinet.setStationName("00:0E:F0:05:06:A9", "mein-fb", False)
	client.profinet.resetStationIPAddress("00:0E:F0:05:06:A9", False)
	client.profinet.setStationIPAddress("00:0E:F0:05:06:A9", "172.21.0.231", "255.255.255.0", "0.0.0.0", False)

	cfg = {
		"vendor_id": 0x014D,
		"device_id": 0x0101,
		"modules": [
			# DAP CU R20
			{
				"ident": 0x00000406,
				"params": [{ "index": 1, "data": B(b"\x21\x00\x1F\x00\x31\x00") }]
			},
			# FB34
			{ "ident": 0x000000D8 },
			# 8DI
			{
				"ident": 0x08000002,
				"input": 1,
				"params": [
					{ "index": 1, "data": B(b"\x01\x00\x07\x01\x51\x00\x00\x00\x00\x00") },
					{ "index": 2, "data": B(b"\x01\x06\x01\x00\xFF") }
				]
			},
			# 4DO
			{
				"ident": 0x00040003,
				"output": 1,
				"params": [
					{ "index": 1, "data": B(b"\x01\x00\x06\x06\x00\x00\x00\x00\x00\xFF") },
					{ "index": 2, "data": B(b"\x12\x00\x00\x00\x00\x00\x00\x00\x00") }
				]
			},
			# FVDA-P2
			{
				"ident": 0x060608C1,
				# I/O length including PROFIsafe header
				"input": 6,
				"output": 6,
				"params": [
					{ "index": 1, "data": B(b"\x01\x00\x08\x04\x00\x00\x00\x00\x00\x7F\x00") },
					{ "index": 2, "data": B(b"\x01\x06\x01\x7F") },
					{ "index": 3, "data": B(b"\x01\x07\x01\x00\x41\x0C\x0A\xFF") },
					{ "index": 1001, "f_dest_add": 0xF }
				]
			},
		]
	}
	print("Requested device configuration:", json.dumps(cfg, sort_keys=True, indent=4, cls=Base64JSONEncoder))

	client.profinet.connect("172.21.0.231", 0x8100, 2, 128, cfg)

	# It's quite possible to get a different configuration back from the
	# device
	print("Actual device configuration:", json.dumps(client.profinet.getDeviceConfiguration(), sort_keys=True, indent=4))

	client.profinet.writeParameters(cfg)

	client.profinet.parameterEnd()

# Crude solution for waiting for application ready. Correct implementation
# would poll with a timeout (or don't give a shit)
time.sleep(0.1)
print("Application ready:", client.profinet.isApplicationReady())

start = time.time()
while (time.time() - start) < 5:
	client.profinet.setOutput(3, B(b"\x55"))
	time.sleep(1)
	print(client.profinet.getInput(2).data)
	client.profinet.setOutput(3, B(b"\xAA"))
	time.sleep(1)
	print(client.profinet.getInput(2).data)

# Now bring the safety module up
print(client.profinet.getSafetyState(4))
print(client.profinet.getSafetyInput(4).data)
client.profinet.operatorAcknowledge(4)
time.sleep(0.5)
print(client.profinet.getSafetyState(4))
print(client.profinet.getSafetyInput(4).data)
time.sleep(0.5)
client.profinet.setSafetyOutput(4, B(b"\x07\x00"))
time.sleep(5)
print(client.profinet.getSafetyState(4))
print(client.profinet.getSafetyInput(4).data)

client.profinet.passivate(4)
time.sleep(2)
print(client.profinet.getSafetyState(4))
print(client.profinet.getSafetyInput(4).data)

# And shut down
client.profinet.release()
client.profinet.stop()

