/*
 *   Copyright 2014 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.xmlrpc;

import static de.colin.profinet.xmlrpc.Converter.convertDCPInfo;
import static de.colin.profinet.xmlrpc.Converter.convertDeviceConfiguration;
import static de.colin.profinet.xmlrpc.Converter.convertMACAddress;
import static de.colin.profinet.xmlrpc.Converter.convertParameterRecordConfiguration;

import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.colin.profinet.AbstractControllerHandler;
import de.colin.profinet.Controller;
import de.colin.profinet.config.ConfigurationVisitor;
import de.colin.profinet.config.DeviceConfiguration;
import de.colin.profinet.config.F_ParameterRecordConfiguration;
import de.colin.profinet.config.ModuleConfiguration;
import de.colin.profinet.config.ParameterRecordConfiguration;
import de.colin.profinet.config.SubmoduleConfiguration;
import de.colin.profinet.io.Frame;
import de.colin.profinet.io.IODataObject;
import de.colin.profinet.pnrt.DCPInfo;
import de.colin.profisafe.ProfisafeHost;
import de.colin.profisafe.ProfisafeHostHandler;
import de.colin.timer.DefaultClock;

/**
 * An interface to a single controller instance. Pure polling based operation.
 * 
 * <p>
 * XML-RPC in its original specification has no notion of {@literal null} and
 * all functions have to return a value. In good old C spirit all functions
 * return 0 for success if they have no proper return value. Errors are always
 * returned as faults (exceptions in XML-RPC lingo).
 * 
 * <p>
 * To make the common case simple, most functions have at least two versions:
 * 
 * <ol>
 * <li>The full function with all arguments
 * <li>A simplified version for the common case where a module has only one
 * submodule. Sometimes they also lack some seldom used arguments. The subslot
 * index is always assumed to be 1.
 * <ol>
 * 
 * <p>
 * There's almost no support for APIs != 0, except for
 * {@link #read(int, int, int, int, int) read} and
 * {@link #write(int, int, int, int, byte[]) write}. Chances are high that the
 * API argument is useless in this context, because the device configuration
 * always assumes API 0.
 */
public final class Functions {

    private static class Safety {
        boolean passivated;
        ProfisafeHost host;
    }

    private static DefaultClock clock;
    private static Controller controller;
    private static Frame inputFrame;
    private static Frame outputFrame;
    // Maps module/submodule slots to PROFISAFE hosts
    private static Map<Integer, Safety> psHosts;
    private static ProfisafeHostHandler psHostHandler;

    synchronized static void init() throws IOException {
        assert controller == null;

        clock = new DefaultClock();
        clock.update();

        controller = new Controller("virtual-plc",
                new AbstractControllerHandler() {
                    @Override
                    public void onIOInput(Controller controller,
                            long timestamp, Frame frame) {
                        clock.update();

                        synchronized (controller) {
                            // Forward the request to the PROFIsafe hosts
                            int iodIndex = -1;
                            for (IODataObject iod : frame.getIODataObjects()) {
                                iodIndex += 1;

                                ByteBuffer slice = frame.dataSlice(iodIndex);
                                inputFrame.setDataGood(iodIndex,
                                        frame.isDataGood(iodIndex));
                                inputFrame.dataSlice(iodIndex).put(slice);
                                slice.flip();

                                int key = iod.getSlot() << 16
                                        | iod.getSubslot();

                                Safety safety = psHosts.get(key);
                                if (safety != null) {
                                    if (frame.isDataGood(iodIndex)) {
                                        safety.host.readProfisafeInput(slice);
                                    }
                                }
                            }
                        }
                    }

                    @Override
                    public void onIOOutput(Controller controller,
                            long timestamp, Frame frame) {
                        clock.update();

                        synchronized (controller) {
                            for (int iocsIndex = 0; iocsIndex < frame
                                    .getIOConsumerStatus().length; iocsIndex++) {
                                frame.setConsumerDataGood(iocsIndex, true);
                            }

                            // Forward the request to the PROFIsafe hosts
                            int iodIndex = -1;
                            for (IODataObject iod : frame.getIODataObjects()) {
                                iodIndex += 1;

                                ByteBuffer slice = frame.dataSlice(iodIndex);
                                frame.setDataGood(iodIndex,
                                        outputFrame.isDataGood(iodIndex));
                                slice.put(outputFrame.dataSlice(iodIndex));
                                slice.flip();

                                int key = iod.getSlot() << 16
                                        | iod.getSubslot();

                                Safety safety = psHosts.get(key);
                                if (safety == null) {
                                    continue;
                                }

                                if (!safety.passivated) {
                                    safety.host.writeProfisafeOutput(slice);
                                } else {
                                    while (slice.hasRemaining()) {
                                        slice.put((byte) 0);
                                    }
                                }
                                frame.setDataGood(iodIndex, true);
                            }
                        }
                    }
                });

        psHostHandler = new ProfisafeHostHandler() {
            @Override
            public void onWatchdogTimeout(ProfisafeHost host) {
            }

            @Override
            public void onOANecessary(ProfisafeHost host) {
            }

            @Override
            public void onFVActivated(ProfisafeHost host) {
            }

            @Override
            public void onConnected(ProfisafeHost host) {
            }

            @Override
            public void onCRCError(ProfisafeHost host) {
            }
        };
        psHosts = new HashMap<Integer, Safety>();
    }

    // -- Ethernet --

    /**
     * Starts the PN-RT frame handling. This enables the DCP methods and allows
     * you to {@link #connect(String, int, int, int, Map) connect} to a device.
     * 
     * @param localAddress
     *            local IP address
     * 
     * @return 0
     */
    public int start(String localAddress) throws IOException {
        synchronized (controller) {
            controller.start(
                    new InetSocketAddress(InetAddress.getByName(localAddress),
                            0x8894), false);
        }
        return 0;
    }

    /**
     * @return 0
     */
    public int stop() throws IOException {
        synchronized (controller) {
            controller.stop();
        }
        return 0;
    }

    public boolean isStopped() {
        return controller.isStopped();
    }

    public boolean isConnected() {
        return controller.isConnected();
    }

    /**
     * Returns whether application ready has been received and both stations
     * exchange data.
     */
    public boolean isRunning() {
        return controller.isRunning();
    }

    /**
     * @return "virtual-plc"
     */
    public String getName() {
        return controller.getName();
    }

    // -- DCP --

    /**
     * Returns the information returned by the device through DCP:
     * 
     * <dl>
     * <dt>(pnrt_address)?
     * <dd>MAC address of station for PN-RT data
     * <dt>(vendor_id)?
     * <dt>(device_id)?
     * <dt>(name_of_station)?
     * <dt>(type_of_station)?
     * <dt>(role)?
     * <dd>One of <code>DEVICE</code> or <code>CONTROLLER</code>.
     * <dt>(ip)?
     * <dt>(netmask)?
     * <dt>(gateway)?
     * </dl>
     * 
     * <p>
     * <em>Beware that all items are optional.</em>
     * 
     * @param name
     *            station name
     * 
     * @return the information tuple
     */
    public Map<String, Object> findStationByName(String name)
            throws IOException {
        DCPInfo info;
        synchronized (controller) {
            info = controller.findStationByName(name);
        }
        return convertDCPInfo(info);
    }

    /**
     * Returns an array of known stations which have been seen an unspecified
     * time ago.
     * 
     * <p>
     * The array contains tuples of information as described in
     * {@link #findStationByName(String) findStationByName}.
     * 
     * @return array of tuples with DCP information
     * 
     * @see #findStationByName(String)
     */
    public List<Map<String, Object>> getCachedStations() {
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        List<DCPInfo> stations = new ArrayList<DCPInfo>();

        synchronized (controller) {
            controller.getCachedStations(stations);
        }

        for (DCPInfo info : stations) {
            result.add(convertDCPInfo(info));
        }

        return result;
    }

    /**
     * @param address
     *            MAC address of station
     * @return 0
     */
    public int signalStation(String address) throws IOException {
        synchronized (controller) {
            controller.signalStation(convertMACAddress(address));
        }
        return 0;
    }

    /**
     * @param address
     *            MAC address of station
     * @return 0
     */
    public int resetStationToFactoryDefaults(String address) throws IOException {
        synchronized (controller) {
            controller
                    .resetStationToFactoryDefaults(convertMACAddress(address));
        }
        return 0;
    }

    /**
     * @param address
     *            MAC address of station
     * @return 0
     */
    public int setStationName(String address, String name, boolean permanent)
            throws IOException {
        synchronized (controller) {
            controller.setStationName(convertMACAddress(address), name,
                    permanent);
        }
        return 0;
    }

    /**
     * @param address
     *            MAC address of station
     * @return 0
     */
    public int setStationIPAddress(String address, String ip, String netmask,
            String gateway, boolean permanent) throws IOException {
        synchronized (controller) {
            controller.setStationIPAddress(convertMACAddress(address),
                    Inet4Address.getByName(ip),
                    Inet4Address.getByName(netmask),
                    Inet4Address.getByName(gateway), permanent);
        }
        return 0;
    }

    /**
     * @param address
     *            MAC address of station
     * @return 0
     */
    public int resetStationIPAddress(String address, boolean permanent)
            throws IOException {
        synchronized (controller) {
            controller.resetStationIPAddress(convertMACAddress(address),
                    permanent);
        }
        return 0;
    }

    // -- PN-IO --

    /**
     * Connects to a PROFINET device. {@code outputFrameID} must be a unique (on
     * the same link) and valid PN-IO frame ID for data. Same goes for the alarm
     * reference which must be unique on the link.
     * 
     * <p>
     * The I/O data exchange is started as soon as {@link #parameterEnd()} is
     * called.
     * 
     * <p>
     * The device configuration has the following format:
     * 
     * <dl>
     * <dt>vendor_id
     * <dt>product_id
     * <dt>(modules[n].slot)?
     * <dd>Module slot. If not provided, the list index is used (starting with
     * 0)
     * <dt>modules[n].ident
     * <dt>(modules[n].slot)?
     * <dt>(modules[n].submodules[m].subident)?
     * <dd>By default this is 1
     * <dt>(modules[n].submodules[m].subslot)?
     * <dd>If not provided, the list index is used (starting with 1)
     * <dt>(modules[n].submodules[m].input)?
     * <dt>(modules[n].submodules[m].output)?
     * <dt>(modules[n].submodules[m].params)?
     * <dt>modules[n].submodules[m].params[i].index
     * <dt>modules[n].submodules[m].params[i].data
     * <dd>Data of the parameter record. If the parameter record is a PROFIsafe
     * record, this key is ignored
     * </dl>
     * 
     * <p>
     * For safety submodules you'll have to add at least one parameter record
     * with an <code>f_dest_add</code> key:
     * 
     * <dl>
     * <dt>modules[n].submodules[m].params[i].index
     * <dt>modules[n].submodules[m].params[i].f_dest_add
     * <dd>F-Destination address
     * <dt>(modules[n].submodules[m].params[i].f_wd_time)?
     * <dd>F-Watchdog timeout. Default is 1000 (1 second)
     * <dt>(modules[n].submodules[m].params[i].f_source_add)?
     * <dt>(modules[n].submodules[m].params[i].f_prm_flag1)?
     * <dt>(modules[n].submodules[m].params[i].f_prm_flag2)?
     * <dt>(modules[n].submodules[m].params[i].f_iparcrc)?
     * </dl>
     * 
     * <p>
     * If the module contains only one submodule (<em>very</em> common), you can
     * skip the submodules array and provide all submodule keys to the module
     * instead.
     * 
     * @param host
     *            the hostname of the device
     * @param outputFrameID
     *            valid and unique PROFINET frame ID, e.g. 0x8100
     * @param alarmRef
     *            valid PROFINET alarm reference, e.g. 2
     * @param sendClock
     *            the send clock in milliseconds. Must be one of the valid
     *            PROFINET send clocks
     * @param config
     *            the device configuration
     * 
     * @return 0
     */
    public int connect(String host, int outputFrameID, int alarmRef,
            int sendClock, Map<String, Object> config) throws IOException {

        DeviceConfiguration devConfig = convertDeviceConfiguration(config);

        synchronized (controller) {
            controller.connect(new InetSocketAddress(host, 0x8894),
                    outputFrameID, alarmRef, sendClock, devConfig);

            Frame output = controller.getOutputFrame();
            outputFrame = new Frame(output.getLength(),
                    output.getIOConsumerStatus(), output.getIODataObjects());
            outputFrame.setBytes(new byte[outputFrame.getLength() + 6]);

            Frame input = controller.getInputFrame();
            inputFrame = new Frame(input.getLength(),
                    input.getIOConsumerStatus(), input.getIODataObjects());
            inputFrame.setBytes(new byte[inputFrame.getLength() + 6]);

            psHosts.clear();

            devConfig.accept(new ConfigurationVisitor() {
                int slot;
                SubmoduleConfiguration submodule;

                @Override
                public void visitSubmodule(SubmoduleConfiguration submodule) {
                    this.submodule = submodule;
                }

                @Override
                public void visitParameterRecord(
                        ParameterRecordConfiguration record) {
                    if (!(record instanceof F_ParameterRecordConfiguration)) {
                        return;
                    }

                    F_ParameterRecordConfiguration frecord = (F_ParameterRecordConfiguration) record;

                    // No problem generating a key like this, because slots and
                    // subslots are unsigned shorts in the PN universe
                    int key = slot << 16 | submodule.getSubslot();

                    ProfisafeHost host = new ProfisafeHost(clock, submodule
                            .getInputLength() - 4,
                            submodule.getOutputLength() - 4, psHostHandler);

                    host.setiParCRC(frecord.getiParCRC());
                    host.setHostAddress(frecord.getHostAddress());
                    host.setDeviceAddress(frecord.getDeviceAddress());
                    host.setPrmFlag1(frecord.getPrmFlag1());
                    host.setPrmFlag2(frecord.getPrmFlag2());
                    host.setWatchdogTimeout(frecord.getWatchdogTimeout());
                    host.restart();

                    Safety safety = new Safety();
                    safety.host = host;
                    safety.passivated = false;

                    psHosts.put(key, safety);
                }

                @Override
                public void visitModule(ModuleConfiguration module) {
                    slot = module.getSlot();
                }

                @Override
                public void visitDevice(DeviceConfiguration device) {
                }
            });
        }

        return 0;
    }

    /**
     * Raw disconnect, the device is not notified. If possible, use
     * {@link #release()} instead.
     * 
     * @return 0
     */
    public int disconnect() {
        synchronized (controller) {
            controller.disconnect();
        }
        return 0;
    }

    /**
     * Proper PROFINET disconnect.
     * 
     * @return 0
     */
    public int release() throws IOException {
        synchronized (controller) {
            controller.release();
        }
        return 0;
    }

    public boolean isApplicationReady() throws IOException {
        synchronized (controller) {
            return controller.isApplicationReady();
        }
    }

    /**
     * Returns the device configuration as a structure:
     * 
     * <dl>
     * <dt>vendor_id
     * <dt>device_id
     * <dt>modules[n].slot
     * <dt>modules[n].ident
     * <dt>modules[n].state
     * <dd>One of <code>NO_MODULE</code>, <code>WRONG_MODULE</code>,
     * <code>PROPER_MODULE</code> or <code>SUBSTITUTE</code>
     * <dt>modules[n].submodules[m].subslot
     * <dt>modules[n].submodules[m].subident
     * <dt>modules[n].submodules[m].subsstate
     * <dd>One of <code>NO_SUBMODULE</code>, <code>WRONG_SUBMODULE</code>,
     * <code>PROPER_SUBMODULE</code> or <code>SUBSTITUTE</code>
     * <dt>(modules[n].submodules[m].input)?
     * <dd>Length of input image in bytes if applicable
     * <dt>(modules[n].submodules[m].output)?
     * <dd>Length of output image in bytes if applicable
     * </dl>
     * 
     * @return a device configuration
     */
    public Map<String, Object> getDeviceConfiguration() {
        synchronized (controller) {
            return convertDeviceConfiguration(controller
                    .getDeviceConfiguration());
        }
    }

    /**
     * Writes all parameters from the given device configuration. See
     * {@link #connect(String, int, int, int, Map) connect} for the format of
     * the configuration.
     * 
     * @return 0
     * 
     * @see #connect(String, int, int, int, Map)
     */
    public int writeParameters(Map<String, Object> config) throws IOException {
        DeviceConfiguration devConfig = convertDeviceConfiguration(config);

        synchronized (controller) {
            for (ModuleConfiguration m : devConfig.getModules()) {
                for (SubmoduleConfiguration sm : m.getSubmodules()) {
                    for (ParameterRecordConfiguration pr : sm
                            .getParameterRecords()) {
                        controller.write(0, m.getSlot(), sm.getSubslot(),
                                pr.getIndex() & 0xFFFF,
                                ByteBuffer.wrap(pr.getData()));
                    }
                }
            }
        }

        return 0;
    }

    /**
     * Writes the parameters to the given submodule. See
     * {@link #connect(String, int, int, int, Map) connect} for the format of
     * the parameters.
     * 
     * @return 0
     */
    @SuppressWarnings("unchecked")
    public int writeParameters(int slot, int subslot, Object[] params)
            throws IOException {
        List<ParameterRecordConfiguration> prConfig = new ArrayList<ParameterRecordConfiguration>();

        for (Object p : params) {
            prConfig.add(convertParameterRecordConfiguration((Map<String, Object>) p));
        }

        synchronized (controller) {
            for (ParameterRecordConfiguration pr : prConfig) {
                controller.write(0, slot, subslot, pr.getIndex() & 0xFFFF,
                        ByteBuffer.wrap(pr.getData()));
            }
        }

        return 0;
    }

    /**
     * @see #writeParameters(int, int, Object[])
     */
    public int writeParameters(int slot, Object[] params) throws IOException {
        return writeParameters(slot, 1, params);
    }

    /**
     * @return 0
     */
    public int parameterEnd() throws IOException {
        synchronized (controller) {
            controller.parameterEnd();
        }
        return 0;
    }

    /**
     * Writes a record to the given submodule.
     * 
     * @param api
     *            API of the record. Usually 0
     * @param slot
     *            module slot
     * @param subslot
     *            submodule slot. Usually 1
     * @param index
     *            record index
     * @param data
     *            record data
     * @return 0
     */
    public int write(int api, int slot, int subslot, int index, byte[] data)
            throws IOException {
        synchronized (controller) {
            controller.write(api, slot, subslot, index, ByteBuffer.wrap(data));
        }
        return 0;
    }

    /**
     * @see #write(int, int, int, int, byte[])
     */
    public int write(int slot, int index, byte[] data) throws IOException {
        return write(0, slot, 1, index, data);
    }

    /**
     * Reads a record from the given submodule. Returns the following tuple:
     * 
     * <dl>
     * <dt>length
     * <dd>Actual record length
     * <dt>data
     * <dd>Requested data
     * </dl>
     * 
     * @param api
     *            API of the record. Usually 0
     * @param slot
     *            module slot
     * @param subslot
     *            submodule slot. Usually 1
     * @param index
     *            record index
     * @param length
     *            requested length
     * @return a tuple
     */
    public Map<String, Object> read(int api, int slot, int subslot, int index,
            int length) throws IOException {
        ByteBuffer data = ByteBuffer.allocate(length);

        int recordLength;
        synchronized (controller) {
            recordLength = controller.read(api, slot, subslot, index, data);
        }

        Map<String, Object> result = new HashMap<String, Object>();
        byte[] bytes = new byte[data.remaining()];
        data.get(bytes);
        // Length can be different from what has been requested
        result.put("length", recordLength);
        result.put("data", bytes);

        return result;
    }

    /**
     * @see #read(int, int, int, int, int)
     */
    public Map<String, Object> read(int slot, int index, int length)
            throws IOException {
        return read(0, slot, 1, index, length);
    }

    // -- I/O --

    /**
     * Sets the output data for the default submodule in the given slot. The
     * data valid flag is set to true.
     * 
     * <p>
     * If you have to set the data-good flag, use
     * {@link #setOutput(int, int, boolean, byte[])} instead.
     * 
     * @return 0
     * 
     * @see #setOutput(int, int, boolean, byte[])
     */
    public int setOutput(int slot, byte[] data) {
        synchronized (controller) {
            if (!controller.isConnected()) {
                throw new IllegalStateException("Not connected to device");
            }

            assert outputFrame != null;

            int index = outputFrame.findIODIndex(slot, 1);
            if (index == -1) {
                throw new IllegalArgumentException(
                        "Couldn't find output image for slot " + slot);
            }

            outputFrame.dataSlice(index).put(data);
            outputFrame.setDataGood(index, true);
        }

        return 0;
    }

    /**
     * @return 0
     */
    public int setOutput(int slot, int subslot, boolean good, byte[] data) {
        synchronized (controller) {
            if (!controller.isConnected()) {
                throw new IllegalStateException("Not connected to device");
            }

            assert outputFrame != null;

            int index = outputFrame.findIODIndex(slot, subslot);
            if (index == -1) {
                throw new IllegalArgumentException(
                        "Couldn't find output image for slot " + slot
                                + ", subslot " + subslot);
            }

            outputFrame.dataSlice(index).put(data);
            outputFrame.setDataGood(index, good);
        }

        return 0;
    }

    /**
     * Returns the inpurt image for the default submodule in the given slot.
     * 
     * <p>
     * If you need the data-good flag, use {@link #getInput(int, int)} instead.
     * 
     * @return the input image
     * 
     * @see #getInput(int, int)
     */
    public byte[] getInput(int slot) {
        byte[] data;
        synchronized (controller) {
            if (!controller.isConnected()) {
                throw new IllegalStateException("Not connected to device");
            }

            assert inputFrame != null;

            int index = inputFrame.findIODIndex(slot, 1);
            if (index == -1) {
                throw new IllegalArgumentException(
                        "Couldn't find input image for slot " + slot);
            }

            data = new byte[inputFrame.getIODataObjects()[index].getLength()];

            inputFrame.dataSlice(index).get(data);
        }
        return data;
    }

    /**
     * Returns the following tuple for the input image of the given submodule:
     * 
     * <dl>
     * <dt>good
     * <dd>data-good flag of the input image
     * <dt>data
     * <dd>the input image
     * </dl>
     */
    public Map<String, Object> getInput(int slot, int subslot) {
        byte[] data;
        boolean good;

        synchronized (controller) {
            if (!controller.isConnected()) {
                throw new IllegalStateException("Not connected to device");
            }

            assert inputFrame != null;

            int index = inputFrame.findIODIndex(slot, subslot);
            if (index == -1) {
                throw new IllegalArgumentException(
                        "Couldn't find input image for slot " + slot
                                + ", subslot " + subslot);
            }

            data = new byte[inputFrame.getIODataObjects()[index].getLength()];

            inputFrame.dataSlice(index).get(data);
            good = inputFrame.isDataGood(index);
        }

        Map<String, Object> result = new HashMap<String, Object>();
        result.put("data", data);
        result.put("good", good);
        return result;
    }

    // -- Safety --

    private static ProfisafeHost getProfisafeHost(int slot, int subslot) {
        return getSafety(slot, subslot).host;
    }

    private static Safety getSafety(int slot, int subslot) {
        int key = slot << 16 | subslot;

        Safety safety = psHosts.get(key);
        if (safety == null) {
            throw new IllegalArgumentException("No PROFIsafe module in slot "
                    + slot + ", subslot " + subslot);
        }

        return safety;
    }

    /**
     * Sets the safe process output image for the given submodule. If the
     * submodule is passivated the image will not be transfered until it's
     * depassivated again.
     * 
     * @return 0
     */
    public int setSafetyOutput(int slot, int subslot, byte[] data) {
        synchronized (controller) {
            getProfisafeHost(slot, subslot).setProcessOutput(data);
        }
        return 0;
    }

    public byte[] getSafetyInput(int slot, int subslot) {
        byte[] input;

        synchronized (controller) {
            ProfisafeHost host = getProfisafeHost(slot, subslot);
            input = new byte[host.getProcessInputLength()];
            getProfisafeHost(slot, subslot).getProcessInput(input);
        }

        return input;
    }

    /**
     * Returns a string describing the current state of the PROFIsafe
     * connection:
     * 
     * <dl>
     * <dt>OA_NECESSARY
     * <dd>Device needs an OA to reintegrate (switch to PV). In "pure" PROFIsafe
     * the OA is only needed to reintegrate after a communication problem, but
     * to support module passivation it's also necessary to latch activate_FV
     * until OA has been triggered.
     * <dt>INIT
     * <dd>Disconnected - Never connected
     * <dt>CE_CRC
     * <dd>Disconnected - Host or device detected an CRC error
     * <dt>WD_TIMEOUT
     * <dd>Disconnected - Host or device encountered a watchdog timeout
     * <dt>DEVICE_FAULT
     * <dd>Connected/FV - Device reports a device fault
     * <dt>FV_ACTIVATED
     * <dd>Connected/FV - Device uses failsafe values
     * <dt>DATA_EX
     * <dd>Connected/PV - Device uses process values
     * </dl>
     */
    public String getSafetyState(int slot, int subslot) {
        synchronized (controller) {
            ProfisafeHost host = getProfisafeHost(slot, subslot);

            // Disconnected states

            if (host.isCRCError()) {
                return "CE_CRC";
            }

            if (host.isWatchdogTimeout()) {
                return "WD_TIMEOUT";
            }

            if (host.isOANecessary()) {
                return "OA_NECESSARY";
            }

            if (!host.isConnected()) {
                return "INIT";
            }

            // Everything below is connected

            if (host.isDeviceFault()) {
                return "DEVICE_FAULT";
            }

            if (host.isFVActivated()) {
                return "FV_ACTIVATED";
            }
        }

        return "DATA_EX";
    }

    /**
     * Triggers one operator acknowledge (auto-reset)
     * 
     * @return 0
     */
    public int operatorAcknowledge(int slot, int subslot) {
        synchronized (controller) {
            getProfisafeHost(slot, subslot).operatorAcknowledge();
        }
        return 0;
    }

    /**
     * Stops the host from sending safety PDUs to the given submodule. Instead
     * the device will receive all 0s.
     * 
     * @return 0
     */
    public int passivate(int slot, int subslot) {
        synchronized (controller) {
            Safety safety = getSafety(slot, subslot);
            safety.passivated = true;
        }
        return 0;
    }

    /**
     * Starts the sending of safety PDUs to the given submodule.
     * 
     * @return 0
     */
    public int depassivate(int slot, int subslot) {
        synchronized (controller) {
            Safety safety = getSafety(slot, subslot);
            safety.passivated = false;
        }
        return 0;
    }

    public boolean isPassivated(int slot, int subslot) {
        synchronized (controller) {
            return getSafety(slot, subslot).passivated;
        }
    }

    /**
     * @see #setSafetyOutput(int, int, byte[])
     */
    public int setSafetyOutput(int slot, byte[] data) {
        return setSafetyOutput(slot, 1, data);
    }

    /**
     * @see #getSafetyInput(int, int)
     */
    public byte[] getSafetyInput(int slot) {
        return getSafetyInput(slot, 1);
    }

    /**
     * @see #getSafetyState(int, int)
     */
    public String getSafetyState(int slot) {
        return getSafetyState(slot, 1);
    }

    /**
     * @see #operatorAcknowledge(int, int)
     */
    public int operatorAcknowledge(int slot) {
        return operatorAcknowledge(slot, 1);
    }

    /**
     * @see #passivate(int, int)
     */
    public int passivate(int slot) {
        return passivate(slot, 1);
    }

    /**
     * @see #depassivate(int, int)
     */
    public int depassivate(int slot) {
        return depassivate(slot, 1);
    }

    /**
     * @see #isPassivated(int, int)
     */
    public boolean isPassivated(int slot) {
        return isPassivated(slot, 1);
    }

}
