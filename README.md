CPF3 controller with XML-RPC interface
======================================

This program uses the `de.colin.profinet` library to provide a single PROFINET
controller with a XML-RPC to control its state.

It has been developed to control and test a device through a Python script.

License
-------

The code is licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0).

Disclaimer
----------

PROFINET and PROFIsafe are trademarks of the [PROFIBUS Nutzerorganisation e.V.
(PNO)](http://www.profibus.com/).

