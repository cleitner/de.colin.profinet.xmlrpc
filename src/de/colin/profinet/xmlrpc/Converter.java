/*
 *   Copyright 2014 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.xmlrpc;

import java.net.InetAddress;
import java.net.SocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.colin.profinet.config.DeviceConfiguration;
import de.colin.profinet.config.F_ParameterRecordConfiguration;
import de.colin.profinet.config.ModuleConfiguration;
import de.colin.profinet.config.ParameterRecordConfiguration;
import de.colin.profinet.config.SubmoduleConfiguration;
import de.colin.profinet.pnrt.DCPInfo;
import de.colin.profinet.pnrt.MACAddress;

/**
 * Converts Profinet classes to and from XML-RPC data types
 */
public final class Converter {

    private Converter() {
    }

    public static MACAddress convertMACAddress(String mac) {
        Pattern p = Pattern.compile("^((?:0[xX])?\\p{XDigit}{1,2}):"
                + "((?:0[xX])?\\p{XDigit}{1,2}):"
                + "((?:0[xX])?\\p{XDigit}{1,2}):"
                + "((?:0[xX])?\\p{XDigit}{1,2}):"
                + "((?:0[xX])?\\p{XDigit}{1,2}):"
                + "((?:0[xX])?\\p{XDigit}{1,2})$");

        Matcher m = p.matcher(mac);
        if (!m.matches()) {
            throw new IllegalArgumentException("Invalid MAC address");
        }

        byte[] address = new byte[6];
        for (int n = 0; n < 6; n++) {
            address[n] = (byte) Integer.parseInt(m.group(1 + n), 16);
        }

        return new MACAddress(address);
    }

    public static String convertAddress(SocketAddress address) {
        return address.toString();
    }

    public static String convertAddress(InetAddress address) {
        byte[] raw = address.getAddress();
        return String.format("%d.%d.%d.%d", raw[0] & 0xFF, raw[1] & 0xFF,
                raw[2] & 0xFF, raw[3] & 0xFF);
    }

    public static Map<String, Object> convertDCPInfo(DCPInfo info) {
        Map<String, Object> result = new HashMap<String, Object>();
        if (info == null) {
            return result;
        }

        if (info.getPNRTAddress() != null) {
            result.put("pnrt_address", convertAddress(info.getPNRTAddress()));
        }
        if (info.getVendorID() != -1) {
            result.put("vendor_id", info.getVendorID());
        }
        if (info.getDeviceID() != -1) {
            result.put("device_id", info.getDeviceID());
        }
        if (info.getNameOfStation() != null) {
            result.put("name_of_station", info.getNameOfStation());
        }
        if (info.getTypeOfStation() != null) {
            result.put("type_of_station", info.getTypeOfStation());
        }
        if (info.getRole() != null) {
            result.put("role", info.getRole().toString());
        }
        if (info.getIPAddress() != null) {
            result.put("ip", convertAddress(info.getIPAddress()));
        }
        if (info.getNetmask() != null) {
            result.put("netmask", convertAddress(info.getNetmask()));
        }
        if (info.getGateway() != null) {
            result.put("gateway", convertAddress(info.getGateway()));
        }

        return result;
    }

    public static ParameterRecordConfiguration convertParameterRecordConfiguration(
            Map<String, Object> dict) {
        int index = (Integer) dict.get("index");

        if (dict.containsKey("f_dest_add")) {
            int deviceAddress = (Integer) dict.get("f_dest_add");
            int prmFlag1 = dict.containsKey("f_prm_flag1") ? (Integer) dict
                    .get("f_prm_flag1") : 0x08;
            int hostAddress = dict.containsKey("f_source_add") ? (Integer) dict
                    .get("f_source_add") : 1000;
            int watchdogTimeout = dict.containsKey("f_wd_time") ? (Integer) dict
                    .get("f_wd_time") : 1000;

            int iParCRC = dict.containsKey("f_iparcrc") ? (Integer) dict
                    .get("f_iparcrc") : 0;
            int prmFlag2;
            if (dict.containsKey("f_prm_flag2")) {
                // User specifically requested PrmFlag2. She is wise and knows
                // what she wants
                prmFlag2 = (Integer) dict.get("f_prm_flag2");
            } else {
                // User has no idea or doesn't care, so we set the iParCRC used
                // flag depending on the data provided
                prmFlag2 = 0x40;
                if (dict.containsKey("f_iparcrc")) {
                    prmFlag2 = (prmFlag2 & 0xC7) | (1 << 3);

                    // Also set the check iParCRC flag if PrmFlag1 hasn't been
                    // set
                    if (!dict.containsKey("f_prm_flag1")) {
                        prmFlag1 |= 0x02;
                    }
                }
            }

            return new F_ParameterRecordConfiguration((short) index, prmFlag1,
                    prmFlag2, hostAddress, deviceAddress, watchdogTimeout,
                    iParCRC);
        } else {
            byte[] data = (byte[]) dict.get("data");
            return new ParameterRecordConfiguration((short) index, data);
        }
    }

    @SuppressWarnings("unchecked")
    public static SubmoduleConfiguration convertSubmoduleConfiguration(
            int subslot, Map<String, Object> dict) {

        subslot = (Integer) (dict.containsKey("subslot") ? dict.get("subslot")
                : subslot);
        int inputLength = (Integer) (dict.containsKey("input") ? dict
                .get("input") : 0);
        int outputLength = (Integer) (dict.containsKey("output") ? dict
                .get("output") : 0);

        // If this configuration is part of a simplified module configuration,
        // we have to switch the ident-key
        int ident = (Integer) (dict.containsKey("subident") ? dict
                .get("subident") : 0);

        List<ParameterRecordConfiguration> parameterRecords = new ArrayList<ParameterRecordConfiguration>();
        if (dict.containsKey("params")) {
            for (Object p : (Object[]) dict.get("params")) {
                parameterRecords
                        .add(convertParameterRecordConfiguration((Map<String, Object>) p));
            }
        }

        return new SubmoduleConfiguration(subslot, ident, inputLength,
                outputLength, null, parameterRecords);
    }

    @SuppressWarnings("unchecked")
    public static ModuleConfiguration convertModuleConfiguration(int slot,
            Map<String, Object> dict) {

        slot = (Integer) (dict.containsKey("slot") ? dict.get("slot") : slot);
        int ident = (Integer) dict.get("ident");

        List<SubmoduleConfiguration> submodules = new ArrayList<SubmoduleConfiguration>();
        int subslot = 1;

        if (dict.containsKey("submodules")) {
            for (Map<String, Object> subdict : (List<Map<String, Object>>) dict
                    .get("submodules")) {
                submodules.add(convertSubmoduleConfiguration(subslot, subdict));
                subslot += 1;
            }
        } else {
            submodules.add(convertSubmoduleConfiguration(subslot, dict));
        }

        return new ModuleConfiguration(slot, ident, submodules);
    }

    @SuppressWarnings("unchecked")
    public static DeviceConfiguration convertDeviceConfiguration(
            Map<String, Object> config) {

        int vendorId = (Integer) config.get("vendor_id");
        int deviceId = (Integer) config.get("device_id");

        List<ModuleConfiguration> modules = new ArrayList<ModuleConfiguration>();
        int slot = 0;
        for (Object m : (Object[]) config.get("modules")) {
            modules.add(convertModuleConfiguration(slot,
                    (Map<String, Object>) m));
            slot = slot + 1;
        }

        return new DeviceConfiguration(vendorId, deviceId, modules);
    }

    public static Map<String, Object> convertSubmoduleConfiguration(
            SubmoduleConfiguration submoduleConfig) {
        Map<String, Object> config = new HashMap<String, Object>();
        if (submoduleConfig == null) {
            return config;
        }

        config.put("subslot", submoduleConfig.getSubslot());
        config.put("subident", submoduleConfig.getIdentNumber());
        config.put("substate", submoduleConfig.getState().toString());

        if (submoduleConfig.getInputLength() >= 0) {
            config.put("input", submoduleConfig.getInputLength());
        }
        if (submoduleConfig.getOutputLength() >= 0) {
            config.put("output", submoduleConfig.getOutputLength());
        }

        return config;
    }

    public static Map<String, Object> convertModuleConfiguration(
            ModuleConfiguration moduleConfig) {
        Map<String, Object> config = new HashMap<String, Object>();
        if (moduleConfig == null) {
            return config;
        }

        config.put("slot", moduleConfig.getSlot());
        config.put("ident", moduleConfig.getIdentNumber());
        config.put("state", moduleConfig.getState().toString());

        List<Map<String, Object>> submodules = new ArrayList<Map<String, Object>>();
        for (SubmoduleConfiguration m : moduleConfig.getSubmodules()) {
            submodules.add(convertSubmoduleConfiguration(m));
        }
        config.put("submodules", submodules);

        return config;
    }

    public static Map<String, Object> convertDeviceConfiguration(
            DeviceConfiguration devConfig) {

        Map<String, Object> config = new HashMap<String, Object>();
        if (devConfig == null) {
            return config;
        }

        config.put("vendor_id", devConfig.getVendorId());
        config.put("device_id", devConfig.getDeviceId());

        List<Map<String, Object>> modules = new ArrayList<Map<String, Object>>();
        for (ModuleConfiguration m : devConfig.getModules()) {
            modules.add(convertModuleConfiguration(m));
        }
        config.put("modules", modules);

        return config;
    }
}
