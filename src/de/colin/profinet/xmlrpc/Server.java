/*
 *   Copyright 2014 Colin Leitner
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package de.colin.profinet.xmlrpc;

import java.net.InetAddress;

import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.RequestProcessorFactoryFactory;
import org.apache.xmlrpc.server.XmlRpcServer;
import org.apache.xmlrpc.server.XmlRpcServerConfigImpl;
import org.apache.xmlrpc.webserver.WebServer;

import de.colin.profinet.pnrt.EthernetUtils;

public class Server {

    public static void main(String[] args) throws Exception {
        String host = "";
        int port = 47819;

        if (args.length == 0) {
            // Defaults are OK
        } else if (args.length == 1) {
            // Port
            port = Integer.parseInt(args[0]);
        } else if (args.length == 2) {
            // Host and port
            host = args[0];
            port = Integer.parseInt(args[1]);
        } else {
            System.err.printf("usage: %s [(<port>|<host> <port>)]%n",
                    Server.class.getName());
            System.exit(1);
            return;
        }

        // Check if we can execute the pnrt-transport helper and can see some
        // interfaces. No need to continue if we have permission problems or the
        // transport is missing altogether
        if (EthernetUtils.getAvailableInterfaces().getInterfaces().isEmpty()) {
            System.err
                    .println("No network interfaces found. It's likely that you lack the permissions for raw access to the network interfaces");
            System.exit(2);
            return;
        }

        WebServer webServer = new WebServer(port, InetAddress.getByName(host));
        XmlRpcServer xmlRpcServer = webServer.getXmlRpcServer();
        PropertyHandlerMapping phm = new PropertyHandlerMapping();

        Functions.init();

        XmlRpcServerConfigImpl serverConfig = (XmlRpcServerConfigImpl) xmlRpcServer
                .getConfig();
        // Only enable these extensions to test some interop. However it's best
        // to support plain XML-RPC for best compatibility
        // serverConfig.setEnabledForExtensions(true);
        serverConfig.setContentLengthOptional(false);

        phm.setRequestProcessorFactoryFactory(new RequestProcessorFactoryFactory.StatelessProcessorFactoryFactory());
        phm.addHandler("profinet", Functions.class);
        xmlRpcServer.setHandlerMapping(phm);

        webServer.start();
    }

}
